import React, { Component } from 'react';

class Grid extends Component {
	render () {
		const { list, display } = this.props;

		return list.map((item, ids) => {
			let displayItems = display(item);
			return <div key={ids}>{displayItems.map(displayItem => displayItem.render)}</div>;
		})
	}
}

export default Grid;
