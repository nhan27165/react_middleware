import React, { Component } from 'react';
import Grid from '../../../common/component/grid'

class List extends Component {
	removeItem = (event, id) => {
		const { onRemove } = this.props;
		
		onRemove(id);
		event.preventDefault();
	}
	
	render () {
		const { todos } = this.props

		return (
			<div>
				<Grid 
				list={todos}
				display={(item, ids) => ([ 
					{ render: <span key={`${ids}-${Math.random()}`}>{`${item.id}/ ` || '--'}</span> },
					{ render: <span key={`${ids}-${Math.random()}`}>{item.task || '--'}</span> },
					{ render: <button key={`${ids}-${Math.random()}`} onClick={(event) => {this.removeItem(event, item.id)}}>---REMOVE----</button> }
				])}
				/>
			</div>
		);
	}
}

export default List;
