import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import './static/css/app.css';
import {
  BrowserRouter as Router,
  Route,
  Link
} from 'react-router-dom'
import Todo from './pages/todo';

const BasicExample = () => (
  <Router >
    <div>
      <ul>
        <li><Link to="/">Home</Link></li>
        <li><Link to="/todo">Todo</Link></li>
      </ul>

      <hr/>

      <Route exact path="/" component={Home2}/>
      
	  <Route path="/todo" component={Todo}/>
	  <Route path="/todo/:id" component={Todo}/>

    </div>
  </Router>
)

class Home extends Component {
  constructor(props) {
    super(props)

    this.state = {
      cursor: 'zoom-in',
      clicked: false
    }
    this.image = null

    this.dragging = false
    this.offsetLeft = 0
    this.offsetTop = 0
    this.newOffsetLeft = 0
    this.newOffsetTop = 0
    this.isLeftIncreasing = null
    this.isTopIncreasing = null
  }
   getTranslate3d (transform) {
    let values = transform.split(/translate3d\(|\);?/)
    if (!values[1] || !values[1].length) {
      return []
    }
    return values[1].split(/,\s?/g)
  }
  onMouseDown(e) {
    e.preventDefault()
    this.image.style.transition = 'unset'
    let [ translateX = '0px', translateY = '0px', translateZ = '0px' ] = this.getTranslate3d(this.image.style.transform)
    this.offsetLeft = e.clientX - Number(translateX.replace('px', ''))
    this.offsetTop = e.clientY - Number(translateY.replace('px', ''))
    this.setState({clicked: true})
  }
  onMouseMove(e) {
    const { clicked, cursor } = this.state

    if (!clicked || cursor !== 'move') return

    this.dragging = true
    const newLeft = e.clientX - this.offsetLeft
    const newTop = e.clientY - this.offsetTop
    if (newLeft < this.newOffsetLeft) {
      this.isLeftIncreasing = false
    } else if (newLeft > this.newOffsetLeft) {
      this.isLeftIncreasing = true
    }
    if (newTop < this.newOffsetTop) {
      this.isTopIncreasing = false
    } else if (newTop > this.newOffsetTop) {
      this.isTopIncreasing = true
    }
    this.newOffsetLeft = newLeft
    this.newOffsetTop = newTop
    this.image.style.transform = `translate3d(${this.newOffsetLeft}px, ${this.newOffsetTop}px, 0px) scale(1)`
  }
  onMouseUp() {
    const { cursor } = this.state
    if (!this.dragging) {
      if (cursor === 'zoom-in') {
        this.setState({cursor: 'move', clicked: false})
      } else {
        this.setState({cursor: 'zoom-in', clicked: false})
      }
      return
    }
    this.imageFlow()
    this.dragging = false
    this.setState({clicked: false})
  }
  imageFlow() {
    let flowLeft = 0
    let flowTop = 0
    this.image.style.transition = 'transform 0.3s linear'
    console.log(this.newOffsetLeft < this.offsetLeft, this.newOffsetLeft,this.offsetLeft)
    console.log(this.newOffsetTop < this.offsetTop, this.newOffsetTop,this.offsetTop)
    switch(this.isLeftIncreasing) {
      case true:
        flowLeft = this.newOffsetLeft + 50
        break
      case false:
        flowLeft = this.newOffsetLeft - 50
        break
      default:
        flowLeft = this.newOffsetLeft
    }
    switch(this.isTopIncreasing) {
      case true:
        flowTop = this.newOffsetTop + 50
        break
      case false:
        flowTop = this.newOffsetTop - 50
        break
      default:
        flowTop = this.newOffsetTop
    }
    this.image.style.transform = `translate3d(${flowLeft}px, ${flowTop}px, 0px) scale(1)`
  }
  render() {
    const { cursor } = this.state
    let style = {}
    style.width = '60%'
    style.cursor = cursor
    style.top = 0
    style.left = 0
    if (cursor === 'move') {
      //style.width = 'unset'
    }

    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to React</h1>
        </header>
        <div className="modal">
          <div className="content">
              <div className="wrapper">
                  <img
                    style={style}
                    src="https://www.californiabeaches.com/wp-content/uploads/2014/09/bigs-Sunset-at-Wipeout-Beach-La-Jolla-California-e1482282928458.jpg"
                    alt="beach"
                    ref={(image) => this.image = image}
                    onMouseDown={(e) => this.onMouseDown(e)}
                    onMouseMove={(e) => this.onMouseMove(e)}
                    onMouseUp={() => this.onMouseUp()}
                  />
              </div>
          </div>
        </div>
      </div>
    )
  }
}

class Home2 extends Component {
  constructor(props) {
    super(props)

    this.bigImg = null
    this.imgWrapper = 0
  }
  onMouseEnter(e) {
    console.log(e.clientX,e.clientY)
    this.bigImg.style.opacity = 1
  }
  onMouseMove(e) {
    this.bigImg.style.left = `${-e.clientX + this.imgWrapper.offsetLeft}px`
    this.bigImg.style.top = `${-e.clientY + this.imgWrapper.offsetTop}px`
  }
  onMouseLeave(e) {
    this.bigImg.style.opacity = 0
  }
  render(){
    return (
    <div
      onMouseEnter={(e) => this.onMouseEnter(e)}
      onMouseMove={(e) => this.onMouseMove(e)}
      onMouseLeave={(e) => this.onMouseLeave(e)}
      style={{position: 'fixed', overflow: 'hidden', width: '555px', margin: 'auto', top: 400, left: 400}}
      ref={(wrapper) => this.imgWrapper = wrapper}
    >
      <img
        src="https://www.californiabeaches.com/wp-content/uploads/2014/09/bigs-Sunset-at-Wipeout-Beach-La-Jolla-California-e1482282928458.jpg"
        width="555"
        alt="Daisy on the Ohoopee"
        className="smallImg"
        ref={(img) => this.smallImg = img}
      />
      <img
        role="presentation"
        src="https://www.californiabeaches.com/wp-content/uploads/2014/09/bigs-Sunset-at-Wipeout-Beach-La-Jolla-California-e1482282928458.jpg"
        className="zoomImg"
        style={{position: 'absolute', top: '0px', left: '0px', opacity: 0, width: '1110px', border: 'none', maxWidth: 'none', maxHeight: 'none', transition: 'opacity 0.5s ease-in-out'}}
        ref={(img) => this.bigImg = img}
      />
    </div>
    )
  }
}

class App extends Component {
  render() {
    return (
      <BasicExample />
    );
  }
}

export default App;
